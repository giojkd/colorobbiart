<?php
$imgPath = 'assets/images/';
$faviconPath = 'assets/favicon/';
$sections = [

  [
    'id' => 1,
    'label' => 'Home',
    'background' => $imgPath.'background-section-1.jpg',
    'background-mobile' => $imgPath.'section-1-mobile.jpg',
    'text' => [
      '1' =>'Per professione o per passione',
      '2' => 'Color of the month'
    ]
  ],
  [
    'id' => 2,
    'label' => 'Ultime news',
    'background' => $imgPath.'background-composed.jpg',
    'text' => [
      '1' => '<span class="text-uppercase">Scopri le<br>ultime<br>novità di<br>colorobbia art</span>',
      '2' => '<span class="text-uppercase"><b>Colorobbia art</b></span>, si rivolge a<br>chiunque voglia cimentarsi<br>con argille e colori, per<br>divertimento o per iniziare<br>una nuova professione.',
      '3' => 'Il nostro compito è aiutarti<br>ad esprimere l\'Artista che è<br>in the attraverso questo<br>catalogo.'
    ],
    'file' => [
      '1' => [
        'label' => 'Bellissimo Glaze',
        'url' => 'assets/catalogs/BELLISSIMO GLAZE.pdf'
      ],
      '2' => [
        'label' => 'Fairy Dust',
        'url' => 'assets/catalogs/FAIRY DUST.pdf'
      ],
      '3' => [
        'label' => 'Terrabella',
        'url' => 'assets/catalogs/TERRABELLA.pdf'
      ]
    ],
    'panel' => [
      '1' => [
        'label' => '',
        'url' => $imgPath.'panel-1.png',
        'href' => 'assets/catalogs/BELLISSIMO GLAZE.pdf'
      ],
      '2' => [
        'label' => '',
        'url' => $imgPath.'panel-2.png',
        'href' => 'assets/catalogs/FAIRY DUST.pdf'
      ],
      '3' => [
        'label' => '',
        'url' => $imgPath.'panel-3.png',
        'href' => 'assets/catalogs/TERRABELLA.pdf'
      ]
    ]
  ],
  [
    'id' => 3,
    'label' => 'Chi siamo',
    'background' => $imgPath.'background-composed.jpg',
    'text' =>[
      '1' => 'Benvenuto sul nostro sito!',
      '2' => 'La vostra <span class="text-uppercase hc">Creatività</span><br>trova spazio nella<br>nostra versatilità.',
      '3' => '<span class="text-uppercase fw-500">Colorobbia</span> produttore da quasi cento<br>anni di argille, colori e smalti, con il brand<br>Colorobbia Art rende il "fare" cercamica<br>facile ed accessibile a tutti quelli che<br>amano creare, sperimentare, mettersi alla<br>prova per gioco, curiosità o per iniziare<br>una nuova professione.',
      '4' => ''
    ],
    'file' => [
      '1' => [
        'label' => '',
        'url' => $imgPath.'homepage-catalog.png'
      ],
      '2' => [
        'label' => '',
        'url' => $imgPath.'homepage-catalog-2.png'
      ]
    ]
  ],
  [
    'id' => 4,
    'label' => 'Cosa facciamo',
    'background' => $imgPath.'background-composed.jpg',
    'columns' => [
      [
        'icon' => $imgPath.'icona-artschool.png',
        'pretitle' => 'Scopri le nostre competenze',
        'title' => 'Ceramic<br>cafè, pyop e principianti',
        'cover' => $imgPath.'cover-col-1.png',
        'paragraph' => 'Stai pensando di iniziare o ampliare il tuo business facendo ceramica? Vuoi trasformare la tua creatività ed il tuo tempo libero in un’opportunità di lavoro? Contattaci e ti forniremo tutte le indicazioni su ciò che ti serve, con un investimento contenuto.',
        'href' => 'assets/catalogs/PRINCIPIANTI.pdf'
      ],
      [
        'icon' => $imgPath.'icona-beginner.png',
        'pretitle' => 'Scopri le nostre competenze',
        'title' => 'Scuole d\'arte e associazioni culturali',
        'cover' => $imgPath.'cover-col-2.png',
        'paragraph' => 'I nostri prodotti sono certificati CE e U.S.A., lead free, nichel free, completamente atossici e food safe. Possono essere utilizzati, in tutta sicurezza, fin dalle prime attività di manipolazione della Scuola dell’Infanzia ed in laboratori specifici di Art Therapy.',
        'href' => 'assets/catalogs/SCUOLA CERAMICA.pdf'
      ],
      [
        'icon' => $imgPath.'icona-professional.png',
        'pretitle' => 'Scopri le nostre competenze',
        'title' => 'Esperti del<br>settore<br>ceramica',
        'cover' => $imgPath.'cover-col-1.png',
        'paragraph' => 'La ceramica per te non ha segreti? Non hai bisogno di sorprese quando applichi o sperimenti  un prodotto? Cerchi l’argilla di alta qualità? Da professionisti a professionista: produciamo argille e colori per ceramica dal 1921.',
        'href' => 'assets/catalogs/ESPERTI DEL SETTORE E CERAMICA.pdf'
      ],
    ]
  ],
  [
    'id' => 5,
    'label' => 'Catalogo',
    'background' => $imgPath.'background-composed.jpg',
    'text' =>[
      '1' => 'Fare Ceramica è<br>un’Arte, sempre.',
      '2' => '<span class="fw-600">COLOROBBIA ART</span>, si rivolge a<br>chiunque voglia cimentarsi<br>con argille e colori per<br>curiosità, per passione, per<br>divertimento o per iniziare<br>una nuova professione.',
    ],
    'file' => [
      '1' => [
        'label' => '',
        'url' => $imgPath.'homepage-catalog.png'
      ]
    ],
    'catalogs' => [
      [
        'label' => 'Catalogo Generale 2020',
        'icon' => '',
        'url' => 'assets/catalogs/CATALOGO GENERALE 2020.pdf',
        'children' => [
          [
            'label' => 'Colori<br>liquidi',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/1_COLORI_LIQUIDI.pdf',
            'icon' => 'fas fa-palette'
          ],
          [
            'label' => 'Colori in<br>polvere',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/4_COLORI_IN_POLVERE.pdf',
            'icon' => 'fas fa-palette'
          ],
          [
            'label' => 'Aerografia',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2016/05/7_AEROGRAFIA.pdf',
            'icon' => 'fas fa-wind'
          ],
          [
            'label' => 'Oro e<br>lustri',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/2_ORO-E_LUSTRI.pdf',
            'icon' => 'fas fa-palette'
          ],
          [
            'label' => 'Biscotto<br>Ceramico',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/10_BISCOTTO_CERAMICO.pdf',
            'icon' => 'fas fa-coffee'
          ],
          [
            'label' => 'Impasto',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/5_IMPASTI.pdf',
            'icon' => 'fas fa-stream'
          ],
          [
            'label' => 'Art Glazes',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/3_ART_GLAZES.pdf',
            'icon' => 'fas fa-palette'
          ],
          [
            'label' => 'Attrezzi',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/6_ATTREZZI.pdf',
            'icon' => 'fas fa-wrench'
          ],
          [
            'label' => 'Spolvero',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2016/05/9_SPOLVERO.pdf',
            'icon' => 'fas fa-voicemail  fa-rotate-90'
          ],
          [
            'label' => 'Accessori<br>Forni',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/12_ACCESSORI_FORNI.pdf',
            'icon' => 'fas fa-temperature-low'
          ],
          [
            'label' => 'Pennelli',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2016/05/8_PENNELLI.pdf',
            'icon' => 'fas fa-paint-brush'
          ],
          [
            'label' => 'Forni',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/11_FORNI.pdf',
            'icon' => 'fas fa-temperature-low'
          ],
          [
            'label' => 'Accessori<br>per decorazioni',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/13_ACCESSORI_PER_DECORAZIONE.pdf',
            'icon' => 'fas fa-star'
          ],
          [
            'label' => 'Macchinari',
            'url' => 'http://www.colorobbiart.it/wp-content/uploads/2018/03/14_MACCHINARI.pdf',
            'icon' => 'fas fa-cogs'
          ]
          ]],
          [
            'label' => 'Catalogo principianti',
            'url' => 'assets/catalogs/PRINCIPIANTI.pdf',
            'icon' => ''
          ],
          [
            'label' => 'Catalogo scuole',
            'url' => 'assets/catalogs/SCUOLA CERAMICA.pdf',
            'icon' => ''
          ],
          [
            'label' => 'Catalogo esperti',
            'url' => 'assets/catalogs/ESPERTI DEL SETTORE E CERAMICA.pdf',
            'icon' => ''
          ]
        ]
      ],
      [
        'id' => 6,
        'label' => 'Eventi & Gallery',

        'background' => $imgPath.'background-composed-black-right.jpg',
        'galleries' => [
          '1' => [
            'text' => [
              '1' => 'FESTA DELLA<br>CERAMICA<br>PORTONI<br>APERTI 2019',
              '2' => 'SETTEMBRE 2019 - Montelupo Fiorentino ',
              '3' => 'Progetto',
              '4' => 'Marino Moretti 1',
              '5' => 'In questo<br>progetto 1, sono<br>stati utilizzati i<br>colori Terrabella,<br>con l’ausilio di<br>tecniche speciali.'
            ],
            'photos' =>[
              $imgPath.'FESTA DELLA CERAMICA PORTONI APERTI 2019/1.jpg',
              $imgPath.'FESTA DELLA CERAMICA PORTONI APERTI 2019/2.jpg',
              $imgPath.'FESTA DELLA CERAMICA PORTONI APERTI 2019/3.jpg',
              $imgPath.'FESTA DELLA CERAMICA PORTONI APERTI 2019/4.jpg',
            ]
          ],
          '2' => [
            'text' => [
              '1' => 'CONTEMPORARY CERAMIC<br>STUDIOS ASSOCIATION',
              '2' => 'LUGLIO 2019 - Buffalo, New York',
              '3' => 'Progetto',
              '4' => 'Marino Moretti 2',
              '5' => 'In questo<br>progetto 2, sono<br>stati utilizzati i<br>colori Terrabella,<br>con l’ausilio di<br>tecniche speciali.'
            ],
            'photos' =>[
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/1.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/2.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/3.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/4.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/5.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/6.jpg',
              $imgPath.'CONTEMPORARY CERAMIC STUDIOS ASSOCIATION/7.jpg',
            ]
          ],
          '3' => [
            'text' => [
              '1' => 'GRAND TOUR<br>DELLA CERAMICA<br>CLASSICA ITALIANA',
              '2' => 'SETTEMBRE 2019 - Croazia',
              '3' => 'Progetto',
              '4' => 'Marino Moretti 3',
              '5' => 'In questo<br>progetto 3, sono<br>stati utilizzati i<br>colori Terrabella,<br>con l’ausilio di<br>tecniche speciali.'
            ],
            'photos' =>[
              $imgPath.'GRAND TOUR DELLA CERAMICA CLASSICA ITALIANA/1.jpg',
              $imgPath.'GRAND TOUR DELLA CERAMICA CLASSICA ITALIANA/2.jpg',
              $imgPath.'GRAND TOUR DELLA CERAMICA CLASSICA ITALIANA/3.jpg',
              $imgPath.'GRAND TOUR DELLA CERAMICA CLASSICA ITALIANA/4.jpg',
              $imgPath.'GRAND TOUR DELLA CERAMICA CLASSICA ITALIANA/5.jpg',
            ]
          ]
        ]
      ],
      [
        'id' => 7,
        'label' => 'Video',
        'background' => $imgPath.'background-b&n.png',
        'tabs' =>[
          [
            'title' => 'Il colore',
            'paragraph' => 'Colorare non è mai stato così facile e divertente con a Art!Smalti, colori, engobbi e cristalline pronti all’uso, anche in formato squeezer; forni a basso consumo energetico (Kw 2,9).',
            'img' => $imgPath.'cover-colore.png',
            'videoYoutube' => 'hbziMLNcEvQ'
          ],
          [
            'title' => 'La forma',
            'paragraph' => 'Mettiamo a disposizione vari tipi di argille di facile manipolazione e disponibili in piccoli formati.Dalle tradizionali bianche e rosse, alle autoindurenti che non necessitano di cottura. ',
            'img' => $imgPath.'cover-forma.png',
            'videoYoutube' => 'qHg9Zu7Vu54'
          ],
          /*    [
          'title' => 'Birth of shape',
          'paragraph' => 'Dalla naturale maiolica alla più complessa porcellana, raku incluso.',
          'img' => $imgPath.'gallery-3.png'
        ],
        [
        'title' => 'Birth of shape',
        'paragraph' => 'Dalla naturale maiolica alla più complessa porcellana, raku incluso.',
        'img' => $imgPath.'gallery-4.png'
      ],
      [
      'title' => 'Birth of shape',
      'paragraph' => 'Dalla naturale maiolica alla più complessa porcellana, raku incluso.',
      'img' => $imgPath.'gallery-5.png'
    ],
    [
    'title' => 'Birth of shape',
    'paragraph' => 'Dalla naturale maiolica alla più complessa porcellana, raku incluso.',
    'img' => $imgPath.'gallery-1.png'
  ],    */

]
],
[
  'id' => 8,
  'label' => 'Contatti',
  'background' => $imgPath.'background-b&n.png'
],

];

$contactDetails = [
  'phone' => '+39 0571 7081',
  'fax' => '+39 0571 708800',
  'admin_address' => 'Via Pietramarina, 53 - 50059 Sovigliana, Vinci (Firenze) Italia',
  'work_address' => 'Via del Lavoro, 65 50056 Montelupo Fiorentino (Firenze)',
  'email' => 'info@colorobbiart.it',
  'info' => 'Colorobbia Italia S.p.A.<br>
  Sede Legale Via Pietramarina, 53 - 50059 Sovigliana, Vinci (Firenze) Italia<br>
  Capitale Sociale € 1.560.000,00 i.v.<br>
  R.I. Firenze e P.IVA/C.F. IT-01847510482<br>
  R.E.A FI 340991'
];

$socialLinks = [
  [
    'label' => '',
    'icon' => 'facebook',
    'link' => 'https://www.facebook.com/ColorobbiaArt/'
  ],
  [
    'label' => '',
    'icon' => 'youtube',
    'link' => 'https://www.youtube.com/channel/UCDn9-8HHNq3vlBMwsHtw6fg'
  ],
  [
    'label' => '',
    'icon' => 'instagram',
    'link' => 'https://www.instagram.com/colorobbia_art/?igshid=76m5ezio2ocm'
  ],
];

$menu = [
  'footer_1' => [
    'label' => 'About',
    'items' => [
      [
        'label' => 'News',
        'url' => ''
      ],
      [
        'label' => 'Lavora con noi',
        'url' => ''
      ],
      [
        'label' => 'Contatti',
        'url' => ''
      ]
    ]
  ],
  'footer_2' => [
    'label' => 'Legal',
    'items' => [
      [
        'label' => 'Note legali',
        'url' => 'http://www.colorobbiart.it/wp-content/uploads/2019/11/COLR_Informativa-Clienti-e-Fornitori_ITA.pdf'
      ],
      [
        'label' => 'Privacy policy e Coookies',
        'url' => 'http://www.colorobbiart.it/wp-content/uploads/2019/03/COLR_IT-InformativaCookie.pdf'
      ]
    ]
  ]
];


$highLightColor = '#fde231';
$highLightColorName = 'Lemon';
$logoDesktop = $imgPath.'logo-colorobbia-white-art-black.png';
$logoMobile = $logoDesktop;
$logoWhite = $imgPath.'logo-colorobbia-white-art-black.png';
$logoBlack = $imgPath.'logo-colorobbia-black-art-black.png';
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="format-detection" content="telephone=no">

  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:300%7CQuestrial">
  <link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/fonts.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/custom.css">
  <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
  <style media="screen">
  .hc{
    color:<?=$highLightColor?>!important;
  }
  .hcb, .hr-hc{
    background-color: <?=$highLightColor?>!important;
  }
  .hc-box, .catalog-list, h2 div{
    border-color: <?=$highLightColor?>!important;

  }
  .vw-100{
    width: 100vh!important;
  }
  </style>
  <script src="https://kit.fontawesome.com/90b54092fb.js" crossorigin="anonymous"></script>
  <link rel="apple-touch-icon" sizes="57x57" href="<?=$faviconPath?>apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?=$faviconPath?>apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=$faviconPath?>apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?=$faviconPath?>apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=$faviconPath?>apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?=$faviconPath?>apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?=$faviconPath?>apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?=$faviconPath?>apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?=$faviconPath?>apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?=$faviconPath?>android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?=$faviconPath?>favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?=$faviconPath?>favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?=$faviconPath?>favicon-16x16.png">
  <link rel="manifest" href="<?=$faviconPath?>manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?=$faviconPath?>ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
</head>
<body>
  <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
  <div class="preloader">
    <div class="preloader-body">
      <div class="cssload-spinner"><span class="cssload-cube cssload-cube0"></span><span class="cssload-cube cssload-cube1"></span><span class="cssload-cube cssload-cube2"></span><span class="cssload-cube cssload-cube3"></span><span class="cssload-cube cssload-cube4"></span><span class="cssload-cube cssload-cube5"></span><span class="cssload-cube cssload-cube6"></span><span class="cssload-cube cssload-cube7"></span><span class="cssload-cube cssload-cube8"></span><span class="cssload-cube cssload-cube9"></span><span class="cssload-cube cssload-cube10"></span><span class="cssload-cube cssload-cube11"></span><span class="cssload-cube cssload-cube12"></span><span class="cssload-cube cssload-cube13"></span><span class="cssload-cube cssload-cube14"></span><span class="cssload-cube cssload-cube15"></span>
      </div>
      <h2 class="preloader-title">Colorobbiart</h2>
    </div>
  </div>
  <div class="page">
    <!-- Page Header-->
    <header class="page-header">
      <!-- RD Navbar-->
      <div class="rd-navbar-wrap">
        <nav class="rd-navbar" data-layout="rd-navbar-sidebar" data-device-layout="rd-navbar-sidebar" data-sm-layout="rd-navbar-sidebar" data-sm-device-layout="rd-navbar-sidebar" data-md-layout="rd-navbar-sidebar" data-md-device-layout="rd-navbar-sidebar" data-lg-layout="rd-navbar-sidebar" data-lg-device-layout="rd-navbar-sidebar" data-xl-layout="rd-navbar-sidebar" data-xl-device-layout="rd-navbar-sidebar" data-xxl-layout="rd-navbar-sidebar" data-xxl-device-layout="rd-navbar-sidebar" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true" data-body-class="rd-navbar-style-1">
          <div class="rd-navbar-main-outer">
            <div class="rd-navbar-main">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <div class="d-block d-md-none vw-100 hcb text-left" style="margin-left: -15px; padding: 20px;">
                  <img class="brand-logo-mobile" src="<?=$logoMobile?>" alt="" srcset=""/>
                </div>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand hcb  d-none d-md-block">
                  <span class="brand">
                    <span class="section-name text-uppercase">01. HOME</span>
                    <img class="brand-logo-desktop" src="<?=$logoDesktop?>" alt="" srcset=""/>
                    <!--<img class="brand-logo-mobile" src="<?=$logoMobile?>" alt="" srcset=""/>-->
                    <!--<img class="brand-logo-mobile" src="images/logo-default-152x65.png" alt="" width="152" height="65" srcset="images/logo-default-304x130.png 2x"/>-->
                  </span>
                </div>
              </div>
              <div class="rd-navbar-nav-wrap">
                <div class="rd-navbar-nav-container">
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <?php foreach ($sections as $key => $section):
                      ?>
                      <li class="rd-nav-item <?=($key == 0) ? 'active' : ''?>"><a class="rd-nav-link" href="#section-<?=($key+1)?>"><?=($key+1)?>. <?=$section['label']?></a>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                  <ul class="contacts-classic">
                    <li>
                      <div class="contacts-classic-title hc">Language:</div><a href="/it">Italiano</a>&nbsp;|&nbsp;<a href="/en">English</a>
                    </li>
                    <li>
                      <div class="contacts-classic-title hc">Phone:</div><a href="tel:#">+39 0571 7081</a>
                    </li>
                    <li>
                      <div class="contacts-classic-title hc">Fax:</div><a href="tel:#">+39 0571 708800</a>
                    </li>
                    <li>
                      <div class="contacts-classic-title hc">E-mail:</div><a href="mailTo:#">info@colorobbiart.it</a>
                    </li>
                    <li>
                      <div class="contacts-classic-title hc">Follow Us:</div>
                      <ul class="list-inline list-social list-inline-sm">
                        <?php foreach ($socialLinks as $key => $link): ?>
                          <li><a class="icon fab fa-<?=$link['icon']?> text-white" href="<?=$link['link']?>"></a></li>
                        <?php endforeach; ?>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <ul class="nav-custom rd-navbar-nav darkBackground">
            <?php foreach ($sections as $key => $section): ?>
              <li class="<?=($key == 0) ? 'active' : ''?>"><a class="nav-custom-counter" href="#section-<?=($key+1)?>"></a></li>
            <?php endforeach; ?>
          </ul>
        </nav>
      </div>
    </header>

    <?php foreach ($sections as $key => $section):
      if(isset($section['text']))
      $text = $section['text'];
      if(isset($section['file']))
      $files = $section['file'];
      if(isset($section['panel']))
      $panels = $section['panel'];
      include './sections/'.$section['id'].'.php';
    endforeach; ?>



    <!-- Page Footer-->
    <?php include './sections/footer.php'; ?>
  </div>
  <div class="snackbars" id="form-output-global"></div>
  <script type="text/javascript">
  var sections = <?=json_encode($sections)?>;
  var logoWhite = '<?=$logoWhite?>';
  var logoBlack = '<?=$logoBlack?>';
  </script>
  <script src="js/core.min.js"></script>
  <script src="js/script.js"></script>
  <!--<script src="js/jquery-visible/jquery.visible.min.js"></script>
  <script type="text/javascript">
  $(function(){
  $(window).scroll(function(){
  console.log($('.darkBackground').visible(false,true));
})
})
</script>-->
<script type="text/javascript">
$(document).ready(function(){


  /* Toggle Video Modal
  -----------------------------------------*/
  function toggle_video_modal() {

    $(".js-trigger-video-modal").on("click", function(e){
      e.preventDefault();
      var id = $(this).attr('data-youtube-id');
      var autoplay = '?autoplay=1';
      var related_no = '&rel=0';
      var src = '//www.youtube.com/embed/'+id+autoplay+related_no;
      $("#youtube").attr('src', src);
      $("body").addClass("show-video-modal noscroll");

    });

    function close_video_modal() {
      event.preventDefault();
      $("body").removeClass("show-video-modal noscroll");
      $("#youtube").attr('src', '');
    }

    $('body').on('click', '.close-video-modal, .video-modal .overlay', function(event) {
      close_video_modal();
    });

    $('body').keyup(function(e) {
      if (e.keyCode == 27) {
        close_video_modal();
      }
    });
  }
  toggle_video_modal();



});
</script>
</body>
</html>
