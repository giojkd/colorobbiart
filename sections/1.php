<!-- DePaletra-->
<section class="d-none d-md-block section position-relative section-full section-inset-1 bg-default text-center bg-image background-position-1 " id="section-1" style="background-image: url(<?=$section['background']?>)">
  <div class="container justifty-content-center">
  <!--  <div class="align-self-center">
      <div class="title-style-1-wrap">
        <div class="oh-desktop wow slideInLeft">
          <h1 class="title-style-1 wow slideInRight">
            <img src="<?=$imgPath?>logo-colorobbia-black.png" alt="" style="margin-bottom: 15px;">
          </h1>
        </div>
      </div>
      <div class="title-style-2-wrap">
        <div class="oh-desktop wow slideInRight">
          <h5 class="title-style-2 wow slideInLeft  p-3"><?=$text['1']?></h5>
        </div>
      </div>
      <a style="font-size: 30px; margin-top: 30px;" class="button button-default wow fadeInUp hcb px-2 text-uppercase border-0" href="about.html"><?=$text['2']?></a>
      <div style="font-size: 24px;" class="mt-4 hc text-uppercase wow slideInLeft fw-700"><?=$highLightColorName?></div>
    </div> -->
  </div>
  <div class="scroll-button darkBackground">
    <span>scroll</span> <i class="fa fa-arrow-down"></i>
  </div>
</section>

<!-- DePaletra-->
<section class="d-block d-md-none section position-relative section-full section-inset-1 bg-default text-center bg-image background-position-1 " id="section-1" style="background: black; margin-top: -65px">
  <div class="justifty-content-center">
  <!--  <div class="align-self-center">
      <div class="title-style-1-wrap">
        <div class="oh-desktop wow slideInLeft">
          <h1 class="title-style-1 wow slideInRight">
            <img src="<?=$imgPath?>logo-colorobbia-black.png" alt="" style="margin-bottom: 15px;">
          </h1>
        </div>
      </div>
      <div class="title-style-2-wrap">
        <div class="oh-desktop wow slideInRight">
          <h5 class="title-style-2 wow slideInLeft  p-3"><?=$text['1']?></h5>
        </div>
      </div>
      <a style="font-size: 30px; margin-top: 30px;" class="button button-default wow fadeInUp hcb px-2 text-uppercase border-0" href="about.html"><?=$text['2']?></a>
      <div style="font-size: 24px;" class="mt-4 hc text-uppercase wow slideInLeft fw-700"><?=$highLightColorName?></div>
    </div> -->
    <img class="img-fluid" src="<?=$section['background-mobile']?>" alt="">
  </div>
  <div class="scroll-button-mobile darkBackground">
    <span>scroll <i class="fa fa-arrow-down"></i></span>
  </div>
</section>
