<!-- About Us-->
<section class="section section-full section-xxl bg-composed-config" id="section-2" style="background-image: url(<?=$section['background']?>)">
  <div class="container">
    <div class="row row-30">
      <div class="col-md-4 col 12">

        <h2 class="wow fadeInLeft fw-600">
          <div>
            <?=$text[1]?>
          </div>

        </h2>
        <div class="hr-black mb-2 mt-2" style="margin-top: 40px!important;"></div>
        <p class="wow fadeInRight">
          <?=$text[2]?>
        </p>

        <p class="wow fadeInLeft" style="margin-top: 20px!important;">
          <?=$text[3]?>
        </p>

        <span class="small-label mt-2 d-block subtitle-classic d-block fw-500 barlow" style="font-size: 16px; margin-top: 40px!important;">Download</span>
        <div class="hr-black mb-2"></div>
        <p>
          <ul>
            <?php foreach ($files as $key => $file): ?>
              <li class="mb-2">
                <a href="<?=$file['url']?>" class="fw-500 text-uppercase"><span class="hc" style="font-size: 15px;"> <i class="fa fa-download"></i> </span> <?=$file['label']?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </p>
      </div>
      <div class="col-md-8 col-12">
        <?php foreach ($panels as $key => $panel): ?>
          <div class="colorobbia-panel hc-box mb-3">
            <a href="<?=$panel['href']?>">
              <img src="<?=$panel['url']?>" alt="">
            </a>

          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

</section>
