<!-- Selected works-->
<section class="section section-full section-md bg-gray-100 section-background-custom" id="section-3" style="background-image: url(<?=$section['background']?>)">
  <div class="container">
    <div class="row row-30">
      <div class="col-12 col-md-9">
        <h2 class="mb-4 fw-600" style="text-transform: none!important; letter-spacing: 4px;!important">
          <div>
            <span class="small"><?=$text['1']?></span>
            <?=$text['2']?>

          </div>
        </h2>
        <div class="d-md-none" style="height: 30px"></div>
        <img class="wow slideInLeft mt-4" src="<?=$files['1']['url']?>" alt="">
      </div>
      <div class="col-12 col-md-3 wow blurIn">
        <p class="mb-4 barlow">
          <?=$text[3]?>
          <br>
          <a href="#"><?=$text[4]?></a>
        </p>
        <div class="hc-box d-inline-block mt-4">
            <img class="" src="<?=$files['2']['url']?>" alt="">
        </div>

      </div>
    </div>
  </div>
  <div class="background-custom-1 bg-default"></div>
</section>
