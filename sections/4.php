<?php
$columns = $section['columns'];

?>
<!-- Our Team-->
<section class="section section-full section-sm bg-default text-md-center" id="section-4">
  <div class="container">

    <div class="row row-lg row-30 justify-content-center">
      <?php foreach ($columns as $key => $column): ?>
        <div class="col-sm-6 col-md-4">
          <div class="column-icon-holder mb-3">
            <img src="<?=$column['icon']?>" alt="">
          </div>
          <h3 class="text-uppercase fw-600 mb-3"><?=$column['title']?></h3>
          <div class="hcb brick mb-4"></div>
          <article class="team-classic box-md hc-box"><img src="<?=$column['cover']?>" alt="" width="370" height="502"/>
            <div class="team-classic-caption">
              <div>

                <div class="team-classic-status"><?=$column['paragraph']?></div>

              </div>
            </div>
          </article>
          <div class="mt-5">
            <a href="<?=$column['href']?>" class="barlow fw-600 text-uppercase ls-3px">
              <span class="hc d-block" style="font-size: 20px; line-height: 40px;"><i class="fa fa-download fa-2x"></i></span>

              Catalogo <?=date('Y')?>
            </a>
          </div>
        </div>
      <?php endforeach; ?>

    </div>
  </div>
</section>
