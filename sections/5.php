
<?php
$catalogs = $section['catalogs'];
?>
<!-- Clients-->
<section class="section section-full bg-gray-100 text-md-center" id="section-5" style="background-image: url(<?=$section['background']?>)">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col-12 col-md-4  text-left offset-md-2">
        <div class="d-flex vh-100" style="height: 100vh">
          <div class="align-self-center">
            <h2 class="wow fadeInLeft fw-600" style="height: 80px;">
              <div>
                <?=$text[1]?>
              </div>
            </h2>
            <div class="hr-black mb-2 mt-2"></div>
            <p class="wow fadeInRight pl-4 pl-md-0">
              <?=$text[2]?>
            </p>
            <div class="p-4 p-md-0">
                <img class="wow slideInLeft mt-4" src="<?=$files['1']['url']?>" alt="">
            </div>

          </div>
        </div>
      </div>
      <div class="col-12 col-md-4  text-left offset-md-2">
        <div class="catalog-list-group p-4 bg-black">
          <h2 class="hc text-uppercase" style="height: 100px">
            <div>
              Catalogo <?=date('Y')?>
            </div>
          </h2>
          <?php foreach ($catalogs as $key => $catalogGroup): ?>
            <h5 class="text-white text-uppercase mb-4">
              <a href="<?=$catalogGroup['url']?>">
                <span class="hc"><i class="far fa-list-alt"></i></span> <?=$catalogGroup['label']?>
              </a>
            </h5>
            <?php if(isset($catalogGroup['children'])):?>
              <div class="catalog-list">
                <ul class="list-unstyled">
                  <?php foreach ($catalogGroup['children'] as $key => $catalog): ?>
                    <li>
                      <a href="<?=$catalog['url']?>" target="_blank" class="text-white d-flex">
                        <span class="hc d-inline-block mr-3 text-center justify-content-center" style="margin-left: -50px; width: 50px; ">
                          <div class="d-flex" style="height: 50px;">
                            <div class="justify-content-center">
                              <i class="fa-2x <?=$catalog['icon']?>"></i>
                            </div>

                          </div>
                        </span>
                        <span class="d-inline-block" style="line-height: 20px;"><?=$catalog['label']?></span>


                      </a>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif;?>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
