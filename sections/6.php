<?php
$galleries = $section['galleries'];
?>
<script type="text/javascript">
  var section6galleries = <?=json_encode($galleries)?>;
  function setSection6Gallery(id){
    $('#section6Text3').html(section6galleries[id].text[3]);
    $('#section6Text4').html(section6galleries[id].text[4]);
    $('#section6Text5').html(section6galleries[id].text[5]);
    console.log(section6galleries[id]);
    $('#section6Gallery .carousel-inner').html('');
    for(var i in section6galleries[id].photos){
      var itemIsActive = (i == 0) ? 'active' : '';
      var newSection6GalleryItem = '<div class="carousel-item '+itemIsActive+'"><img class="d-block w-100" src="'+section6galleries[id].photos[i]+'" alt="First slide"></div>'
      $('#section6Gallery .carousel-inner').append(newSection6GalleryItem);
    }
  }
</script>
<!-- Portfolio-->
<section class="section section-full section-xs section-first bg-default text-md-center bg-composed-config" id="section-6" style="background-image: url(<?=$section['background']?>)">
  <div class="container">
    <div class="row row-lg row-30">
      <div class="col-12 col-md-4 text-left">
        <?php foreach ($galleries as $key => $gallery): ?>
          <div class="mb-4" onclick="setSection6Gallery(<?=$key?>)" style="cursor: pointer;">
            <h2 class="wow fadeInLeft text-uppercases fw-600">
              <div class="">
                <?=$gallery['text'][1]?>
              </div>

            </h2>
            <div class="hr-black mb-2 mt-2"></div>
            <span class="small"><?=$gallery['text'][2]?></span>
          </div>

        <?php endforeach;
        $gallery = $galleries[1];
        ?>
      </div>
      <div class="col-12 col-md-6">
        <div class="row">
          <div class="col-9 text-left">
            <h5 class="text-uppercase mb-2">Gallery</h5>
            <div id="section6Gallery" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php foreach ($gallery['photos'] as $key => $photo): ?>
                  <div class="carousel-item <?=($key == 0) ? 'active' : ''?>">
                    <img class="d-block w-100" src="<?=$photo?>" alt="First slide">
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="text-right">
              <a class="d-block text-white" href="#section6Gallery" role="button" data-slide="next">
                <i class="fas fa-arrow-right fa-3x"></i>
              </a>
            </div>

          </div>
<!--
          <div class="col-4 text-left text-white d-flex align-items-end">
            <div class="align-self-bottom">
              <h5 class="text-white fw-300" id="section6Text3"><?=$gallery['text'][3]?></h5>
              <h3 class="text-white text-uppercase fw-600" id="section6Text4"><?=$gallery['text'][4]?></h3>
              <div class="hr-hc my-3"></div>
              <p id="section6Text5"><?=$gallery['text'][5]?></p>
              <br>
              <a class="d-block text-white" href="#section6Gallery" role="button" data-slide="next">
                <i class="fas fa-arrow-right fa-3x"></i>
              </a>
            </div>
          </div>
-->
        </div>
      </div>
    </div>
  </div>
</section>
