<?php
$tabs = $section['tabs'];
?>

<!-- Portfolio-->
<section class="section section-full section-xs section-first bg-default text-md-center" id="section-7" style="background-image: url(<?=$section['background']?>)">
  <div class="container">
    <div class="row row-lg row-30">
      <?php foreach ($tabs as $key => $tab): ?>
        <div class="col-sm-6 col-md-4 wow blurIn" data-wow-delay=".2s">
          <article class="project-classic box-md"><img src="<?=$tab['img']?>" alt="" width="370" height="262"/>
            <div class="project-classic-caption">
              <div>
                <h4 class="project-classic-title"><a href="single-project.html"><?=$tab['title']?></a></h4>
                <div class="project-classic-tag"><?=$tab['paragraph']?></div>
              </div>
            </div>
          </article>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
