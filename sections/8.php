<section class="section section-full section-xl bg-default" id="section-8" style="background-image: url(<?=$section['background']?>)">
  <div class="container">
    <div class="row">
      <div class="col text-center">
        <div class="p-3  d-inline-block">
          <h1 class="hcb mb-3">Contatti</h1>
          <img style="width:480px;" src="<?=$imgPath?>logo-colorobbia-black-art-black-w800.png" alt="">
        </div>
      </div>
    </div>
    <div class="row row-30 justify-content-center d-flex align-items-end">
      <div class="col-sm-6 col-md-4 wow blurIn" data-wow-delay=".1s">
        <article class="contacts-modern bg-white">
          <div class="contacts-modern-icon fl-bigmug-line-cellphone55"></div>
          <ul class="contacts-modern-list">
            <li><h5 class="hc">Telefono</h5></li>
            <li class="contacts-modern-link"><a href="tel:#"><?=$contactDetails['phone']?></a></li>
            <li><h5 class="hc">Fax</h5></li>
            <li class="contacts-modern-link"><a href="tel:#"><?=$contactDetails['fax']?></a></li>
          </ul>
        </article>
      </div>
      <div class="col-sm-6 col-md-4 wow blurIn">
        <article class="contacts-modern bg-white">
          <div class="contacts-modern-icon fl-bigmug-line-up104"></div>
          <ul class="contacts-modern-list">
            <li><h5 class="hc">Sede amministrativa</h5></li>
            <li class="contacts-modern-link"><?=$contactDetails['admin_address']?></li>
            <li><h5 class="hc">Stabilimento produttivo e logistica</h5></li>
            <li class="contacts-modern-link"><?=$contactDetails['work_address']?></li>
          </ul>
        </article>
      </div>
      <div class="col-sm-6 col-md-4 wow blurIn" data-wow-delay=".1s">
        <article class="contacts-modern bg-white" style="border: 1px solid rgba(17,17,17,0.1)">
          <div class="contacts-modern-icon fl-bigmug-line-chat55"></div>
          <ul class="contacts-modern-list">
            <li><h5 class="hc">Email</h5></li>
            <li class="contacts-modern-link"><a href="mailto:<?=$contactDetails['email']?>"><?=$contactDetails['email']?></a></li>
            <li class="text-white">.</li>
            <li class="text-white">.</li>
          </ul>
        </article>
      </div>
    </div>
  </div>

</section>
<div class="">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d282396.19818699366!2d10.785864188306457!3d43.68340467194902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a5d4fd790cbeb%3A0x6d69ec3983054ed0!2sColorobbia%20S.p.A.!5e0!3m2!1sen!2sit!4v1574932649957!5m2!1sen!2sit" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
