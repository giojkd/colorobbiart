<footer class="section footer-classic section-full section-full-1 section-sm section-inset-2 bg-gray-3 context-dark text-md-center" id="contacts">
  <div class="container">
    <div class="subtitle-classic wow fadeInUp">Mettiti in contatto</div>
    <h3>
      <span class="d-inline-block wow fadeInLeft">lavoriamo</span> <span class="d-inline-block wow fadeInRight">insieme</span>
    </h3>
    <!-- RD Mailform-->
    <form class="rd-form rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
      <div class="form-wrap wow fadeInRight">
        <label class="form-label" for="contact-name">Nome</label>
        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
      </div>
      <div class="form-wrap wow fadeInRight">
        <label class="form-label" for="contact-email">E-Mail</label>
        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Required @Email">
      </div>
      <div class="form-wrap wow fadeInRight">
        <label class="form-label" for="contact-message">Messaggio</label>
        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
      </div>
      <div class="form-button text-center">
        <button class="button button-default wow fadeInUp" type="submit">Invia il messaggio</button>
      </div>
    </form>
  </div>
  <div class="container">
    <div class="footer-classic-list-social wow fadeInUp">
      <ul class="list-inline list-social list-inline-sm">
        <?php foreach ($socialLinks as $key => $link): ?>
          <li><a class="icon fab fa-<?=$link['icon']?>" href="<?=$link['link']?>"></a></li>
        <?php endforeach; ?>
      </ul>
    </div>
    <p class="rights wow fadeInUp"><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>Colorobbia Italia S.p.A.</span><span>.&nbsp;</span></p>
  </div>

</footer>
<div class="subfooter ">
  <div class="container">
    <div class="row">
      <div class="col-md-6 text-white text-left">
        <img class="logoFooter" src="<?=$imgPath?>logo-colorobbia-footer.png" alt="">
        <p class="footer-text mb-3"><?=$contactDetails['info']?></p>
        <form class="footer-form" action="index.html" method="post">
          <input type="email" name="" value="" placeholder="Inserisci qui il tu indirizzo email"> <button type="submit" name="button">Iscriviti alla Newsletter</button>
          <br>
           <p class="footer-text d-inline-block"><input type="checkbox" name="" value=""> Avendo preso visione dell’informativa della privacy acconsento al trattamento dei miei dati personali per l’invio di newsletter e comunicazioni commerciali*</p>
        </form>
      </div>
      <!--<div class="col-md-3" style="padding-top: 80px;">
        <ul class="list-unstyled">
          <h5 class="text-white"><?=$menu['footer_1']['label']?></h5>
          <?php foreach ($menu['footer_1']['items'] as $key => $item): ?>
             <li> <a class="text-white" href="<?=$item['url']?>"> <i class="fas fa-angle-right"></i> <?=$item['label']?></a> </li>
          <?php endforeach; ?>


        </ul>
      </div>-->
      <div class="col-md-3" style="padding-top: 80px;">
        <ul class="list-unstyled ">
          <h5 class="text-white"><?=$menu['footer_2']['label']?></h5>
          <?php foreach ($menu['footer_2']['items'] as $key => $item): ?>
             <li> <a class="text-white" href="<?=$item['url']?>"> <i class="fas fa-angle-right"></i> <?=$item['label']?></a> </li>
          <?php endforeach; ?>

        </ul>
      </div>
    </div>
  </div>
</div>
